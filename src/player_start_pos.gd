extends ColorRect

var player_obj = preload("res://player.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	visible = false
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _input(event):
	if !Input.is_action_just_pressed("start"):
		return
	
	var player_check = 0
	
	#check all players and if none have player_id that matches
	#input device id then create a new player
	for player in get_tree().get_nodes_in_group("players"):
		if event.device == player.player_id:
			player_check += 1
	
	#if player with that id does not exist add new player
	if player_check == 0:
		var player = player_obj.instantiate()
		player.position = position
		player.player_id = event.device
		get_tree().current_scene.add_child(player)
		
