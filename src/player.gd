extends CharacterBody2D


const SPEED = 300.0
const JUMP_VELOCITY = -400.0
var double_jump = true
var direction = 0
var facing = 1
@export var player_id = 1
# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

var bullets = preload("res://bullet.tscn")
var colors = [Color.BLUE,Color.RED,Color.GREEN,Color.PURPLE,Color.ORANGE]

func _ready():
	set_color()
	
func set_color():
	var color = Color.WHITE
	if player_id > colors.size() - 1:
		randomize()
		color = Color(randf(), randf(), randf(), 1)
	else:
		color = colors[player_id]
	$ColorRect.color = color
	
func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta
	else:
		double_jump = true
	if direction:
		velocity.x = direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)

	move_and_slide()
func _input(event):
	if event.device == player_id:
		jump()
		move()
		shoot()
	
func shoot():
	if Input.is_action_just_pressed("shoot"):
		var bullet = bullets.instantiate()
		bullet.position = position
		bullet.direction = facing
		bullet.player_id = player_id
		get_tree().current_scene.add_child(bullet)
		
func move():
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	direction = Input.get_axis("player_left", "player_right")
	if direction != 0:
		facing = direction


func jump():
		# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
	elif Input.is_action_just_pressed("jump") && double_jump:
		double_jump = false
		velocity.y = JUMP_VELOCITY
